import React, { FC, useEffect, useState } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    Button,
    TextInput,
    Keyboard,
    KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';
import { styles } from './styles';
import { setName } from './redux/actions';

interface Props {
    joke?: string;
    setName: Function;
}

export const MainView: FC<Props> = ({
    joke,
    setName
}) => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const dismissKeyboard = () => { Keyboard.dismiss() };
    const onStartShouldSetResponder = () => true;

    const setUserName = () => {
        setName({ firstName, lastName });
    }

    return (
        <SafeAreaView
            style={styles.content}
            onResponderRelease={dismissKeyboard}
            onStartShouldSetResponder={onStartShouldSetResponder}
        >
            <KeyboardAvoidingView style={styles.content} behavior="padding" enabled>
                <Text style={styles.text}>Get random joke</Text>
                <Text style={styles.jokeText}>{joke}</Text>
                <TextInput
                    style={styles.textInput}
                    placeholder={'First Name'}
                    onChangeText={setFirstName}
                    value={firstName}
                />
                <TextInput
                    style={styles.textInput}
                    placeholder={'Last Name'}
                    onChangeText={setLastName}
                    value={lastName}
                />
                <Button
                    title="Get random joke"
                    onPress={setUserName}
                />
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
};

const mapStateToProps = (state: any) => ({
    joke: state.jokeParams.joke
});
const mapDispatchToProps = {
    setName
};
export default connect(mapStateToProps, mapDispatchToProps)(MainView);
