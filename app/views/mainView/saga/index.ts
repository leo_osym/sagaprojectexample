import { setJoke } from './../redux/actions';
import { constants } from './../redux/constants';
import { takeEvery, call, put, select } from 'redux-saga/effects';
import { SagaIterator } from "redux-saga";
import { fetchData } from '../../../services/restApi';

export const getFirstName = (state: any) => state.jokeParams.firstName;
export const getLastName = (state: any) => state.jokeParams.lastName;

export function* getJokeSagaWorker(action: any): SagaIterator {
    //const { firstName, lastName } = action.payload; // we can get from action
    const firstName = yield select(getFirstName); // or we can get from state
    const lastName = yield select(getLastName);
    const joke = yield call(fetchData, firstName, lastName);
    yield put(setJoke(joke));
}

export function* getJokeSagaWatcher(): SagaIterator {
    yield takeEvery(constants.SET_NAME, getJokeSagaWorker);
}