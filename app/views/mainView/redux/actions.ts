import { constants } from './constants';

type SetName = {
    type: string,
    payload: {
        firstName: string, 
        lastName: string
    }
}

type SetJoke = {
    type: string,
    payload: string
} 

export type Action = SetJoke | SetName;

export const setName = (data: { firstName: string, lastName: string }): SetName => {
    return {
        type: constants.SET_NAME,
        payload: data
    }
}

export const setJoke = (data: string): SetJoke => {
    return {
        type: constants.SET_JOKE,
        payload: data
    }
}