import { Action } from './actions';
import { constants } from './constants';

type initState = {
    firstName: string,
    lastName: string,
    joke: string
}

const initState = {
    firstName: '',
    lastName: '',
    joke: ''
}

export default (state: initState = initState, action: Action) => {
    const { type, payload } = action;
    switch (type) {
        case constants.SET_NAME:
            return {
                ...state,
                firstName: typeof payload === 'object' ? payload.firstName : payload,
                lastName: typeof payload === 'object' ? payload.lastName: payload,
            };
        case constants.SET_JOKE:
            return {
                ...state,
                joke: payload
            };
        default:
            return state;
    }
};