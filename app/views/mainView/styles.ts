import { StyleSheet } from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";

export const styles = StyleSheet.create({
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  text: {
    fontSize: 25,
    marginTop: 30,
    marginBottom: 20,
    marginHorizontal: 20,
    fontWeight: 'bold'
  },
  jokeText: {
    fontSize: 20,
    marginBottom: 20,
    marginHorizontal: 20
  },
  textInput: { 
    fontSize: 18,
    width: 350, 
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1,
    marginVertical: 5
  }
});