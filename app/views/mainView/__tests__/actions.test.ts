jest.unmock('redux-mock-store')
jest.unmock('redux-saga')
jest.autoMockOff()

import configureStore from 'redux-mock-store'
import { setName, setJoke } from '../redux/actions';
import { constants } from '../redux/constants';

const mockStore = configureStore([]);
const initState = {
  firstName: '',
  lastName: '',
  joke: ''
}

describe('test actions', () => {
  it('should return actions', () => {
    const expectedActions = [
      {
        payload: {
          firstName: "dean",
          lastName: "moriarty",
        },
        type: constants.SET_NAME
      },
      {
        payload: 'surprise',
        type: constants.SET_JOKE
      }
    ];
    const store = mockStore(initState);
    store.dispatch(setName({ firstName: 'dean', lastName: 'moriarty' }));
    store.dispatch(setJoke('surprise'));
    let retActions = store.getActions();
    expect(retActions).toEqual(expectedActions);
  });
})