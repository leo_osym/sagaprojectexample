import { getFirstName, getLastName } from './../saga/index';
import { setName, setJoke } from './../redux/actions';
import { fetchData } from './../../../services/restApi/index';
import { expectSaga } from 'redux-saga-test-plan';
import { getJokeSagaWorker } from '../saga';
import { call, select } from 'redux-saga-test-plan/matchers';

let firstName = '';
let lastName = '';
let response = '';
describe('get data from api', () => {
    test('exact order with redux-saga-test-plan', () => {
        return expectSaga(getJokeSagaWorker, setName)
        .provide([
            [select(getFirstName), firstName],
            [select(getLastName), lastName],
            [call(fetchData, firstName, lastName), response]
          ])
          .put(setJoke(response))
          .run();
      });
});