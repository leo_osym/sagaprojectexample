import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import { MainView } from '..';

describe('MainView', () => {
    test('renders correctly', () => {
        const renderer = ShallowRenderer.createRenderer();
        const result = renderer.render(<MainView setName={jest.fn()}/>);
        expect(result).toMatchSnapshot();
    });
});