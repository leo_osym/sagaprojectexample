import { constants } from '../redux/constants';
import jokeParams from '../redux/reducers';

const initState = {
    firstName: '',
    lastName: '',
    joke: ''
}

const initMockReducer = {
    payload: '',
    type: ''
};

const mockSetNameReducer = {
    payload: {
        firstName: "dean",
        lastName: "moriarty",
    },
    type: constants.SET_NAME
};

const mockSetJokeReducer = {
    payload: "surprise",
    type: constants.SET_JOKE
};

describe('set joke reducer', () => {
    it('returns the initial state', () => {
        expect(jokeParams(undefined, initMockReducer)).toEqual(initState);
    });

    it('set name', () => {
        expect(jokeParams(initState, mockSetNameReducer,))
        .toEqual({
            ...initState,
            firstName: 'dean',
            lastName: 'moriarty'
        });
    });
    it('set joke', () => {
        expect(jokeParams(initState, mockSetJokeReducer,))
        .toEqual({
            ...initState,
            joke: 'surprise'
        });
    });
});