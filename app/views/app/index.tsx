import React, { FC } from 'react';
import {
    Provider,
} from 'react-redux';
import MainView from '../mainView';
import { store } from '../../services/redux/store';

interface Props {
}

export const App: FC<Props> = ({}) => {
    return (
        <Provider store={store}>
            <MainView />
        </Provider>
    );
};

export default App;
