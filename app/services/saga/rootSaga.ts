import { call, put, takeEvery, takeLatest, all, fork, spawn } from 'redux-saga/effects';
import { getJokeSagaWatcher } from '../../views/mainView/saga/index';
import { SagaIterator } from 'redux-saga';

export default function* rootSaga(): SagaIterator {
    yield all([
        fork(getJokeSagaWatcher),
    ]);
}