// The Internet Chuck Norris Database api: http://www.icndb.com/api/

export const fetchData = async (firstName: string, lastName: string) => {
    let data: string = '';
    try {
        data = await getJoke(firstName, lastName);
    } catch (error) {
        console.error(error);
    }
    return data;
}

const getJoke = async (firstName: string, lastName: string) => {
    let data: string = '';
    let response: any;
    if (firstName && lastName) {
        response = await fetch(`https://api.icndb.com/jokes/random?escape=javascript&limitTo=[nerdy]&firstName=${firstName}&lastName=${lastName}`);

    } else {
        response = await fetch('https://api.icndb.com/jokes/random?escape=javascript&limitTo=[nerdy]');
    }
    let responseJson = await response.json();
    data = responseJson.value.joke;
    return data;
}